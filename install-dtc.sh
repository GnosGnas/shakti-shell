DTC_DIR = $1

tar -xvzf dtc-1.4.7.tar.gz
cd dtc-1.4.7/
make NO_PYTHON=1 PREFIX=/usr/
make install NO_PYTHON=1 PREFIX=/usr/
cd $(DTC_DIR)
rm -rf dtc-1.4.7
