# Path to Vivado executable
## TODO - Automate the updating of bashrc when XIL_DIR changes without rebuilding a new container
XIL_DIR = /home/surya/Vivado/2020.1

# Do not change these parameters for running the docker image!!
IMG_NAME = "shakti-shell:1"
BSV_DIR = /
DTC_DIR = /tmp/
SHAKTI_DIR = /tmp/
VERILATOR_DIR = /tmp/

# docker run - default rule
## TODO - save this as a script for each container so that multiple images can be managed with this Makefile
run_image:
	docker run -it --privileged -v /dev/bus/usb:/dev/bus/usb \
        -v $(HOME):/home/$(USER) \
        -v /etc/passwd:/etc/passwd:ro \
        -v $(XIL_DIR):$(XIL_DIR) \
        -v $(SHAKTI_DIR)/shakti-tools:$(SHAKTI_DIR)/shakti-tools \
        -v $(SHAKTI_DIR)/src/shakti-sdk:$(SHAKTI_DIR)/src/shakti-sdk \
        --user $(id -u):$(id -g) \
        -w $(HOME) \
        $(IMG_NAME)

# Build the shakti-shell image
docker_build_shaktishell: 
	sed -i 's|SHAKTI_DIR=.*|SHAKTI_DIR=$(SHAKTI_DIR)|' bash.bashrc
	sed -i 's|XIL_DIR=.*|XIL_DIR=$(XIL_DIR)|' bash.bashrc
	sed -i 's|BSV_DIR=.*|BSV_DIR=$(BSV_DIR)|' bash.bashrc
	sudo service docker start
	sudo docker build --build-arg BSV_DIR=$(BSV_DIR) --build-arg DTC_DIR=$(DTC_DIR) --build-arg SHAKTI_DIR=$(SHAKTI_DIR) --build-arg VERILATOR_DIR=$(VERILATOR_DIR) --build-arg XIL_DIR=$(XIL_DIR) -t $(IMG_NAME) .

# To uninstall docker in the system
rm_docker:
	for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
	sudo snap remove docker
	sudo apt-get remove docker 
	@echo "Do sudo rm -rf /var/lib/containerd and rm -rf /var/lib/docker to remove built containers"


## From the docker website - Last updated Sep/23
#### To install docker
install_docker: 
    # Add Docker's official GPG key:
	sudo apt-get update
	sudo apt-get -y install ca-certificates curl gnupg
	sudo install -m 0755 -d /etc/apt/keyrings
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
	sudo chmod a+r /etc/apt/keyrings/docker.gpg

    # Add the repository to Apt sources:
	echo \
    "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
	sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
	sudo apt-get update
