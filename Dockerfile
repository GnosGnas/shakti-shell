# Docker file for Shakti compilers and SDK
FROM ubuntu:18.04
ENV DEBIAN_FRONTEND="noninteractive"
ENV TZ="Asia/Kolkata"

# Build arguments - default values have also been given
ARG BSV_DIR=/
ARG DTC_DIR=/tmp/
ARG SHAKTI_DIR=/opt/
ARG VERILATOR_DIR=/tmp/

ENV BSV_DIR=$BSV_DIR
ENV DTC_DIR=/tmp
ENV SHAKTI_DIR=$SHAKTI_DIR
ENV $VERILATOR_DIR=$VERILATOR_DIR


# Install dependencies from user_manual sec. 3.3.2 (Tool installation
# First is for the Bluespec compiler

RUN apt-get update && apt-get install -y \
    ghc libghc-regex-compat-dev libghc-syb-dev iverilog \
    libghc-old-time-dev libfontconfig1-dev libx11-dev \
    libghc-split-dev libxft-dev flex bison libxft-dev \
    tcl-dev tk-dev libfontconfig1-dev libx11-dev gperf \
    itcl3-dev itk3-dev autoconf git

# TODO - This was facing some issue which I couldn't get
RUN cd $BSV_DIR && \
    git clone --recursive https://github.com/B-Lang-org/bsc && \
    cd bsc && \
    make PREFIX=$(pwd) && make install-src

# Python installation
RUN apt-get update
RUN apt-get install -y libtool

RUN apt-get update && apt-get install -y software-properties-common
RUN apt-get update && apt-get install -y python3.6 python3-pip

RUN apt-get install -y autoconf automake autotools-dev curl make-guile \
    libmpc-dev libmpfr-dev libgmp-dev libusb-1.0-0-dev bc \
    gawk build-essential bison flex texinfo gperf libtool \
    patchutils zlib1g-dev pkg-config libexpat-dev \
    libusb-0.1 libftdi1 libftdi1-2 \
    libpython3.6-dev 

# DTC installation
RUN mkdir -p $DTC_DIR
COPY dtc-1.4.7.tar.gz install-dtc.sh $DTC_DIR
RUN cd $DTC_DIR && /bin/bash ./install-dtc.sh $DTC_DIR

# Git clone Shakti-sdk and Shakti-tools
RUN mkdir -p $SHAKTI_DIR
COPY install-shakti.sh $SHAKTI_DIR
RUN cd $SHAKTI_DIR && /bin/bash ./install-shakti.sh $SHAKTI_DIR

# Verilator installation
RUN mkdir -p $VERILATOR_DIR
COPY install-verilator.sh $VERILATOR_DIR
RUN cd $VERILATOR_DIR && /bin/bash ./install-verilator.sh

# Other installations
RUN apt-get install -y libmpc3 libmpfr6 sudo

# bashrc is copied
COPY bash.bashrc /etc/

RUN ln -s /usr/lib/x86_64-linux-gnu/libmpfr.so.6 /usr/lib/x86_64-linux-gnu/libmpfr.so.4

RUN pip3 install repomanager==1.2.1 Cerberus==1.3.1 ruamel.yaml==0.15.97

RUN apt -y install make patchutils zlib1g-dev pkg-config libexpat-dev
