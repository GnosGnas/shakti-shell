SHAKTI_DIR = $1

mkdir -p /src
cd /src
git clone https://gitlab.com/shaktiproject/software/shakti-sdk.git
cd $(SHAKTI_DIR)
git clone --recursive https://gitlab.com/shaktiproject/software/shakti-tools.git

